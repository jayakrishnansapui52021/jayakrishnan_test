## Application Details
|               |
| ------------- |
|**Generation Date and Time**<br>Tue Aug 10 2021 13:36:39 GMT+0530 (India Standard Time)|
|**App Generator**<br>@sap/generator-fiori|
|**App Generator Version**<br>1.3.0|
|**Generation Platform**<br>Visual Studio Code|
|**Floorplan Used**<br>simple|
|**Service Type**<br>OData Url|
|**Service URL**<br>http://cch1wpsap2.corp.hexaware.com:8000/sap/opu/odata/sap/ZEMP_REGISTRATION_SRV
|**Module Name**<br>employee_pec|
|**Application Title**<br>Employee Details|
|**Namespace**<br>com.hex|
|**UI5 Theme**<br>sap_fiori_3|
|**UI5 Version**<br>Latest|
|**Enable Telemetry**<br>True|

## employee_pec

A Fiori application.

### Starting the generated app

-   This app has been generated using the SAP Fiori tools - App Generator, as part of the SAP Fiori tools suite.  In order to launch the generated app, simply run the following from the generated app root folder:

```
    npm start
```

#### Pre-requisites:

1. Active NodeJS LTS (Long Term Support) version and associated supported NPM version.  (See https://nodejs.org)


